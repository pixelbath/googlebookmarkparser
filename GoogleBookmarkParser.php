<?php

class GoogleBookmarkParser
{
    final public static function parseFile($filePath)
    {
        $html = file_get_contents($filePath, false);
        $dom = new DOMDocument();
        $dom->loadHTML($html);

        $dts = $dom->getElementsByTagName('dt');

        // placeholder
        $tagTitle = '';
        $output = array();

        foreach ($dts as $dt) {
            $innerElement = $dt->firstChild;

            // it's a tag
            if ($innerElement->tagName === "h3") {
                $attr = $innerElement->getAttribute('add_date');
                $add_date = self::parseTimestamp($attr);
                $tagTitle = $innerElement->nodeValue;
            }

            // it's a link
            if ($innerElement->tagName === "a") {
                // brute force check to see if we already stored it because of how Google's export works
                $urlExists = false;
                for ($i = 0; $i < count($output); $i++) {
                    $link = $output[$i];
                    
                    if ($link["url"] === $innerElement->getAttribute('href')) {
                        // if link's there, see if we need to add a new tag to it
                        if (array_search($tagTitle, $link["tags"]) === false) {
                            array_push($output[$i]["tags"], $tagTitle);
                        }

                        // nothing more to do, move on to next item
                        $urlExists = true;
                        break;
                    }
                }

                if ($urlExists === false) {
                    // grab notes if present
                    $notes = null;
                    $nextElement = $dt->nextSibling;
                    if ($nextElement !== null && $nextElement->tagName === "dd") {
                        $notes = trim(str_replace("\n", " ", $nextElement->nodeValue));
                    }

                    $add_date = self::parseTimestamp($innerElement->getAttribute('add_date'));

                    $output[] = array(
                        "url" => $innerElement->getAttribute('href'),
                        "title" => $innerElement->nodeValue,
                        "tags" => array($tagTitle),
                        "notes" => $notes,
                        "add_date" => $add_date,
                    );
                }
            }
        }

        return $output;
    }

    private static function parseTimestamp($timestamp) {
        // if it's longer than 13 digits, it's likely in microseconds
        if (strlen($timestamp) > 13) {
            $timestamp = intval($timestamp / 1000000);
        }
        return new DateTime("@{$timestamp}");
    }
}
