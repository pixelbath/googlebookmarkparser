# GoogleBookmarkParser

Since the export format for Google Bookmarks is completely terrible, this is a parser written in PHP for pulling that steaming pile of "HTML" into something usable.

Written to help extricate myself from Google.

## How it works

The output HTML treats tags as top-level organization, so they're listed like headings with the corresponding links under them. The end result of this is bookmarks are duplicated if multiple tags are applied.

This script inverts that model and groups tags under bookmarks.

## Usage

```php
$myData = GoogleBookmarkParser::parseFile("GoogleBookmarks.html");
print_r($myData);

// outputs:
// Array
// (
//     [0] => Array
//         (
//             [url] => http://www.example.com/url1.html
//             [title] => First Example
//             [tags] => Array
//                 (
//                     [0] => examples
//                     [1] => firsties
//                 )

//             [notes] => Some notes for this bookmark
//             [add_date] => DateTime Object
//                 (
//                     [date] => 2008-04-17 14:35:05.000000
//                     [timezone_type] => 1
//                     [timezone] => +00:00
//                 )

//         )

//     [1] => Array
//         (
//             [url] => http://www.example.com/
//             [title] => Other Example
//             [tags] => Array
//                 (
//                     [0] => examples
//                 )

//             [notes] => 
//             [add_date] => DateTime Object
//                 (
//                     [date] => 2008-04-19 16:15:30.000000
//                     [timezone_type] => 1
//                     [timezone] => +00:00
//                 )

//         )
// )
```
