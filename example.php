<?php

require_once('GoogleBookmarkParser.php');

ini_set('display_errors', true);
error_reporting(E_ALL);

$bookmarks = GoogleBookmarkParser::parseFile("GoogleBookmarks.html");

$db = new PDO("mysql:host=localhost;dbname=ExampleDb", "db_username", "db_password");

// error checking is an exercise for the reader
foreach ($bookmarks as $bookmark) {
    $stmt = $db->prepare("select BookmarkID from Bookmark where URL=:url");
    $stmt->execute(array(":url" => $bookmark['url']));

    $result = $stmt->fetchColumn();
    if ($result === false) {
        echo "Bookmark " . $bookmark["title"] . " not found in database. Adding.<br />";

        $sql = "insert into Bookmark (Title, URL, Note, DateCreated) values (:title, :url, :note, :date_created)";
        $stmt = $db->prepare($sql);
        $result = $stmt->execute(array(
            ':title' => $bookmark["title"],
            ':url' => $bookmark["url"],
            ':note' => $bookmark["notes"],
            ':date_created' => $bookmark["add_date"]->format('Y-m-d H:i:s'),
        ));
        
        $bookmarkId = $db->lastInsertId();
    } else {
        $bookmarkId = $result;
    }

    foreach ($bookmark["tags"] as $tag) {
        // does this tag exist? if not, add it
        $stmt = $db->prepare("select TagID from Tag where Tag=:tag");
        $stmt->execute(array(":tag" => $tag));
        $tagId = $stmt->fetchColumn();

        if ($tagId === false) {
            echo "Tag {$tag} not found in database. Adding.<br />";

            $stmt = $db->prepare("insert into Tag (Tag) values (:tag)");
            $stmt->execute(array(':tag' => $tag));
            $tagId = $db->lastInsertId();
        }

        // is it linked already? if not, insert a link
        $stmt = $db->prepare("select BookmarkTagLinkID from BookmarkTagLink where BookmarkID=:bookmark_id and TagID=:tag_id");
        $stmt->execute(array(':bookmark_id' => $bookmarkId, ':tag_id' => $tagId));
        $linkId = $stmt->fetchColumn();

        if ($linkId === false) {
            echo "Linking bookmark to tag.<br />";
            $stmt = $db->prepare("insert into BookmarkTagLink (BookmarkID, TagID) values (:bookmark_id, :tag_id)");
            $stmt->execute(array(
                ':bookmark_id' => $bookmarkId,
                ':tag_id' => $tagId
            ));
        }
    }
    echo "<br />";
}